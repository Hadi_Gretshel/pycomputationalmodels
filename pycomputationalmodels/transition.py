from pycomputationalmodels.state import State


class Transition(object):
    def __init__(self, head_state, tail_state, action):
        """
        Args:
            head_state (State): the head state of the transition
            tail_state (State): the tail state of the transition
            action (Action): the action that is "executed" during the transition
        """

        self.head_state = head_state
        self.tail_state = tail_state
        self.action = action

    def __str__(self):
        return 'Transition(head_state:{head_state}, tail_state:{tail_state}, action:{action})'. \
            format(head_state=self.head_state, tail_state=self.tail_state, action=self.action)

    def __repr__(self):
        return str(self)
