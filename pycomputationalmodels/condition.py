class Condition(object):
    def __init__(self, arg, operator, operand, result):
        """
        Create a condition

        Notes:
            In Equals operations: the operand is the second part of the equation.
            if the equation is true then the result field is 0.
            for not equals the result field is 1

        Examples:
                for the condition "a equals 3" it will be as follows Condition('a',Operator.EQUALS, 3, 0)
                for the condition "b not equals 2" it will be as follows Condition('b',Operator.EQUALS, 2, 1)
                for the condition "c modulo 3 is 0" it will be as follows Condition('c',Operator.MODULO, 3, 0)

        Args:
            arg (str): the char that the condition applies to
            operator (Operator): the operator of the condition
            operand (int | None): the operand of the condition, None if there is no operand
            result (int): the result of the condition
        """
        self.arg = arg
        self.operator = operator
        self.operand = operand
        self.result = result

    def __str__(self):
        return 'Condition(arg:{arg}, operator:{operator}, operand:{operand}, result:{result})'. \
            format(arg=self.arg, operator=self.operator, operand=self.operand,
                   result=self.result)

    def __repr__(self):
        return str(self)
