from enum import Enum


class Action(Enum):
    NO_ACTION = 0
    POP = 1
    PUSH = 2
    MOVE_LEFT = 3
    MOVE_RIGHT = 4
    WRITE = 5
