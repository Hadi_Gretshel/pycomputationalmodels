from pycomputationalmodels.state import State


class Circuit(object):
    def __init__(self, states, transitions, head_states, tail_states):
        self.states = states
        self.transitions = transitions
        self.head_states = head_states
        self.tail_states = tail_states

    def __str__(self):
        return 'Circuit(states:{states}, transitions:{transitions}, head_states:{head_states},' \
               'tail_states={tail_states})'.format(states=self.states,
                                                   transitions=self.transitions,
                                                   head_states=self.head_states,
                                                   tail_states=self.tail_states)
