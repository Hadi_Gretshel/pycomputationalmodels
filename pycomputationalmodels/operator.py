from enum import Enum


class Operator(Enum):
    EQUALS = 0
    MODULO = 1
