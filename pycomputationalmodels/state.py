from pycomputationalmodels.condition import Condition


class State(object):
    def __init__(self, name, conditions):
        """
        Args:
            name (str): the name of the state
            conditions (list[Condition]): the list of the conditions that represent the state
        """

        self.name = name
        self.conditions = conditions
        self.args_values = {condition.arg: condition.result for condition in self.conditions}

    def __str__(self):
        return 'State(name:{name}, conditions:{conditions}, args_values={args_values})' \
            .format(name=self.name, conditions=self.conditions, args_values=self.args_values)

    def __repr__(self):
        return str(self)
